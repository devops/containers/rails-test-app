ARG base_image
FROM ${base_image}

ARG rails_minor

ENV RAILS_MINOR=$rails_minor

WORKDIR /usr/src/app

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install nodejs; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

RUN set -eux; \
	gem install "rails:~>${RAILS_MINOR}.0"; \
	rails new . \
  	  --database=postgresql \
	  --skip-javascript \
	  --skip-bootsnap \
	  --skip-git \
	  --skip-keeps \
	  --skip-listen \
	  --skip-spring \
	  --skip-system-test \
	  --skip-test \
	; \
	./bin/bundle install --deployment --jobs="$(nproc)"; \
	echo '/vendor/bundle' > ./.gitignore; \
	rm -f ./.ruby-version; \
	chmod -R 777 .
