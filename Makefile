SHELL = /bin/bash

ruby_major ?= 2.6
rails_minor ?= 5.2
build_tag ?= rails-$(rails_minor)

base_image = ruby:$(ruby_major)

.PHONY : build
build:
	docker pull $(base_image)
	DOCKER_BUILDKIT=1 docker build -t $(build_tag) \
		--build-arg base_image=$(base_image) \
		--build-arg rails_minor=$(rails_minor) \
		- < ./Dockerfile
