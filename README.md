# Rails Test Application

A generic generated Rails app for automated testing.

Examples

In Dockerfile:

    COPY --from=gitlab-registry.oit.duke.edu/devops/containers/rails-test-app:rails-5.2-ruby-2.6 \
         /usr/src/app [DEST]

Bash:

    $ docker run --rm -d --name rails-test-app \
          gitlab-registry.oit.duke.edu/devops/containers/rails-test-app:rails-5.2-ruby-2.6 \
          sleep infinity)
    $ docker cp rails-test-app:/usr/src/app [DEST]
    $ docker stop rails-test-app
